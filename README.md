# siminau-msglib

A library that defines the RPC protocol messages used by all siminau projects.
Please note that the protocol messages are not tied to any particular RPC
protocol, and it is intended that these messages are usable with any RPC
protocol.

## Getting started

### Install Rust toolchain

To build, first install the Rust toolchain. While the toolchain may be
installable from a package management system depending on your platform, it
currently is best to install via [rustup][1]. Please visit [www.rustup.rs][1]
to download and run the installer.

[1]: https://www.rustup.rs

### Run tests

Once Rust is installed, simply enter these commands to confirm that the test
suite succeeds:

```shell
$ git clone https://github.com/siminau/siminau-msglib.git
$ cd siminau-msglib
$ cargo test
```

This will run all unit, integration, and doc tests.

## Features

* Message structs based on MessagePack

## Licensing

This project is licensed under the MIT license.
