// tests/codeconvert.rs
// Copyright (C) 2018 authors and contributors (see AUTHORS file)
//
// This file is released under the MIT License.

// ===========================================================================
// Externs
// ===========================================================================

// Stdlib externs

// Third-party externs
extern crate failure;
#[macro_use]
extern crate failure_derive;

// Local externs
#[macro_use]
extern crate siminau_msglib_derive;

// ===========================================================================
// Imports
// ===========================================================================

// Stdlib imports

// Third-party imports

// Local imports

// ===========================================================================
// Helpers
// ===========================================================================

#[derive(Fail, Debug)]
#[fail(display = "Unknown code value: {}", code)]
pub struct CodeValueError
{
    pub code: u64,
}

pub trait CodeConvert<T>: Clone + PartialEq
{
    type int_type;

    /// Convert a number to type T.
    fn from_number(num: Self::int_type) -> Result<T, CodeValueError>;

    /// Convert a u64 to type T.
    fn from_u64(num: u64) -> Result<T, CodeValueError>;

    /// Convert type T to a number.
    fn to_number(&self) -> Self::int_type;

    /// Convert type T to a u64.
    fn to_u64(&self) -> u64;

    /// Return the maximum number value
    fn max_number() -> u64;

    /// Cast a u64 number into acceptable int type
    fn cast_number(n: u64) -> Option<Self::int_type>;
}

// ===========================================================================
// Methods
// ===========================================================================

#[test]
fn test_from_u64_u32values()
{
    // GIVEN
    // an enum with variants that are not tuples/structs and
    //     the variants are assigned u32 values and
    //     the enum derives PartialEq, Clone, and CodeConvert
    // and a list of u32 values and
    //     each value in the list exists as a value in the enum
    #[derive(Debug, PartialEq, Clone, CodeConvert)]
    enum U32Value
    {
        One = 70000,
        Two = 85580,
    }

    let test_vals: Vec<u64> = vec![70000, 85580];

    // WHEN
    // U8Value::from_number is called with a valid u8 value
    let mut result: Vec<U32Value> = vec![];
    for i in 0usize..2 {
        let val = U32Value::from_u64(test_vals[i]).unwrap();
        result.push(val);
    }

    // THEN
    // the result is a U32Value variant and
    // the result matches the expected variant specified in the list element
    let val0 = match result[0] {
        U32Value::One => true,
        _ => false,
    };

    let val1 = match result[1] {
        U32Value::Two => true,
        _ => false,
    };

    assert!(val0 && val1);
}

#[test]
fn test_from_u64_u8values()
{
    // GIVEN
    // an enum with variants that are not tuples/structs and
    //     the variants are assigned u8 values and
    //     the enum derives PartialEq, Clone, and CodeConvert
    // and a list of (NoValue, u8) values and
    //     each value in the list matches a value in the enum
    #[derive(Debug, PartialEq, Clone, CodeConvert)]
    enum U8Value
    {
        One = 42,
        Two = 255,
    }

    let test_vals: Vec<u64> = vec![42, 255];

    // WHEN
    // U8Value::from_number is called with a valid u8 value
    let mut result: Vec<U8Value> = vec![];
    for i in 0usize..2 {
        let val = U8Value::from_u64(test_vals[i]).unwrap();
        result.push(val);
    }

    // THEN
    // the result is a U8Value variant and
    // the result matches the expected variant specified in the list element
    let val0 = match result[0] {
        U8Value::One => true,
        _ => false,
    };

    let val1 = match result[1] {
        U8Value::Two => true,
        _ => false,
    };

    assert!(val0 && val1);
}

#[test]
fn test_from_u64_val_notexist()
{
    // GIVEN
    // an enum with variants that are not tuples/structs and
    //     the variants are assigned u8 values and
    //     the enum derives PartialEq, Clone, and CodeConvert
    // and a u64 value that is not used in the enum
    #[derive(Debug, PartialEq, Clone, CodeConvert)]
    enum U8Value
    {
        One = 42,
        Two = 255,
    }

    let bad_value = 9001u64;

    // WHEN
    // U8Value::from_u64() is called with bad_value
    let result = U8Value::from_u64(bad_value);

    // THEN
    // an error is returned
    // and the error is a CodeValueError
    let result_val = match result {
        Ok(_) => false,
        Err(err) => {
            let errmsg = format!("Unknown code value: {}", bad_value);
            assert_eq!(err.to_string(), errmsg);
            true
        }
    };
    assert!(result_val);
}

#[test]
fn test_to_u64()
{
    // GIVEN
    // an enum with variants that are not tuples/structs and
    //     the enum derives PartialEq, Clone, and CodeConvert
    #[derive(Debug, PartialEq, Clone, CodeConvert)]
    enum Test
    {
        One = 1,
        Two = 42,
    }

    // WHEN
    // Test::Two.to_u64() is called
    let result = Test::Two.to_u64();

    // THEN
    // the result is the value assigned to Test::Two
    assert_eq!(result, 42u64);
}

#[test]
fn test_to_number()
{
    // GIVEN
    // an enum with variants that are not tuples/structs and
    //     the enum derives PartialEq, Clone, and CodeConvert
    #[derive(Debug, PartialEq, Clone, CodeConvert)]
    enum Test
    {
        One = 1,
        Two = 42,
    }

    // WHEN
    // Test::Two.to_number() is called
    let result = Test::Two.to_number();

    // THEN
    // the result is the value assigned to Test::Two
    assert_eq!(result, 42u8);
}

#[test]
fn test_max_number()
{
    // GIVEN
    // an enum with unit variants and
    //     the enum derives PartialEq, Clone, and CodeConvert and
    //     the variants except the last one do not have values assigned and
    //     the last variant has the largest value assigned
    #[derive(Debug, PartialEq, Clone, CodeConvert)]
    enum Test
    {
        One,
        Two,
        Three = 99999,
    }

    // WHEN
    // Test::max_number() is called
    let result = Test::max_number();

    // THEN
    // the value assigned to the last variant is returned
    assert_eq!(result, 99999);
}

#[test]
fn test_cast_number_larger_than_max()
{
    // GIVEN
    // an enum with unit variants and
    //     the enum derives PartialEq, Clone, and CodeConvert and
    //     none of the variants are assigned values and
    //     the CodeConvert implementation has int_type = u8 and
    // an integer value that is larger than u8::max_value()
    #[derive(Debug, PartialEq, Clone, CodeConvert)]
    enum Test
    {
        One,
        Two,
        Three,
    }

    let val: u64 = u8::max_value() as u64 + 1;

    // WHEN
    // Test::cast_number() is called with the integer value
    let result = Test::cast_number(val);

    // THEN
    // None is returned
    assert_eq!(result, None);
}

#[test]
fn test_cast_number_less_than_max()
{
    // GIVEN
    // an enum with unit variants and
    //     the enum derives PartialEq, Clone, and CodeConvert and
    //     none of the variants are assigned values and
    //     the CodeConvert implementation has int_type = u8 and
    // a u64 integer value that is less than u8::max_value()
    #[derive(Debug, PartialEq, Clone, CodeConvert)]
    enum Test
    {
        One,
        Two,
        Three,
    }

    let val: u64 = 42;

    // WHEN
    // Test::cast_number() is called with the integer value
    let result = Test::cast_number(val);

    // THEN
    // The integer value is returned
    let passed = match result {
        Some(r) => r == val as u8,
        _ => false,
    };
    assert!(passed);
}

#[test]
fn test_cast_number_equal_to_max()
{
    // GIVEN
    // an enum with unit variants and
    //     the enum derives PartialEq, Clone, and CodeConvert and
    //     none of the variants are assigned values and
    //     the CodeConvert implementation has int_type = u8 and
    // a u64 integer value that is u8::max_value()
    #[derive(Debug, PartialEq, Clone, CodeConvert)]
    enum Test
    {
        One,
        Two,
        Three,
    }

    let val = u8::max_value() as u64;

    // WHEN
    // Test::cast_number() is called with the integer value
    let result = Test::cast_number(val);

    // THEN
    // The integer value is returned as a u8 value
    let passed = match result {
        Some(r) => r == val as u8,
        _ => false,
    };
    assert!(passed);
}

// ===========================================================================
// Variant values
// ===========================================================================

fn check_values<T>(expected: &[(T, u8)])
where
    T: CodeConvert<T, int_type = u8>,
{
    for (variant, val) in expected.iter() {
        let result = match T::from_number(*val) {
            Ok(r) => &r == variant,
            Err(_) => false,
        };
        assert!(result);
    }
}

#[test]
fn no_value()
{
    // GIVEN
    // an enum with variants that are not tuples/structs and
    //     the variants are not assigned any values and
    //     the enum derives PartialEq, Clone, and CodeConvert
    // and a list of (NoValue, u8) values
    #[derive(Debug, PartialEq, Clone, CodeConvert)]
    enum NoValue
    {
        One,
        Two,
        Three,
    }

    let test_vals: Vec<(NoValue, u8)> =
        vec![(NoValue::One, 0), (NoValue::Two, 1), (NoValue::Three, 2)];

    // WHEN
    // NoValue::from_number is called with a valid u8 value

    // THEN
    // the result is a NoValue variant and
    // the result matches the expected variant specified in the list element
    check_values(&test_vals);
}

#[test]
fn with_value()
{
    // GIVEN
    // an enum with variants that are not tuples/structs and
    //     the variants are assigned u8 values and
    //     the enum derives PartialEq, Clone, and CodeConvert
    // and a list of (WithValue, u8) values
    #[derive(Debug, PartialEq, Clone, CodeConvert)]
    enum WithValue
    {
        One = 1,
        Two = 2,
        Three = 3,
    }

    let test_vals: Vec<(WithValue, u8)> = vec![
        (WithValue::One, 1),
        (WithValue::Two, 2),
        (WithValue::Three, 3),
    ];

    // WHEN
    // NoValue::from_number is called with a valid u8 value

    // THEN
    // the result is a NoValue variant and
    // the result matches the expected variant specified in the list element
    check_values(&test_vals);
}

#[test]
fn skip_values()
{
    // GIVEN
    // an enum with variants that are not tuples/structs and
    //     the variants are assigned u8 values and
    //     the last variant's value is not a consecutive value and
    //     the enum derives PartialEq, Clone, and CodeConvert
    // and a list of (SkipValue, u8) values
    #[derive(Debug, PartialEq, Clone, CodeConvert)]
    enum SkipValue
    {
        One = 1,
        Two = 2,
        Three = 42,
    }

    let test_vals: Vec<(SkipValue, u8)> = vec![
        (SkipValue::One, 1),
        (SkipValue::Two, 2),
        (SkipValue::Three, 42),
    ];

    // WHEN
    // NoValue::from_number is called with a valid u8 value

    // THEN
    // the result is a NoValue variant and
    // the result matches the expected variant specified in the list element
    check_values(&test_vals);
}

// ===========================================================================
//
// ===========================================================================
