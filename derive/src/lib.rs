// src/lib.rs
// Copyright (C) 2017 authors and contributors (see AUTHORS file)
//
// This file is released under the MIT License.

// ===========================================================================
// Externs
// ===========================================================================
#![recursion_limit = "1024"]

// Stdlib externs
extern crate proc_macro;
extern crate proc_macro2;

// Third-party externs
extern crate num;

#[macro_use]
extern crate quote;

extern crate syn;

// Local externs

// ===========================================================================
// Modules
// ===========================================================================

mod codeconvert;

// ===========================================================================
// Imports
// ===========================================================================

// Stdlib imports
use proc_macro::TokenStream;

// Third-party imports
use syn::DeriveInput;

// Local imports
use codeconvert::impl_code_convert;

// ===========================================================================
// Helper
// ===========================================================================

fn build_derive<F>(input: TokenStream, expand: F) -> TokenStream
where
    F: Fn(&DeriveInput) -> proc_macro2::TokenStream,
{
    // Parse input tokens into a syntax tree
    let input: DeriveInput = syn::parse(input).unwrap();

    // Build the impl
    let expanded = expand(&input);

    // Hand output tokens back to compiler
    expanded.into()
}

// ===========================================================================
// CodeConvert
// ===========================================================================

#[proc_macro_derive(CodeConvert)]
pub fn code_convert(input: TokenStream) -> TokenStream
{
    build_derive(input, impl_code_convert)
}

// ===========================================================================
//
// ===========================================================================
