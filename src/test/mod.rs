// src/test/mod.rs
// Copyright (C) 2018 authors and contributors (see AUTHORS file)
//
// This file is released under the MIT License.

// ===========================================================================
// Modules
// ===========================================================================

// ===========================================================================
//
// ===========================================================================

#[test]
fn it_works()
{
    assert_eq!(2 + 2, 4);
}

// ===========================================================================
//
// ===========================================================================
